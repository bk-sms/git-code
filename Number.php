<?php

class Number
{
    private $num;
    public function __construct($num){
        $this->num = $num;
    }

    public function __toString()
    {
        return strval($this->num);
    }
}

echo (new Number(10));
